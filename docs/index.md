---
title: "Projects - Jean-Luc Tibaux"
description: "Projects that I contribute/d to: TinyWeatherForecastGermany, Hypatia, Currencies, Simple Chess Clock, Podverse"
---

# :octicons-project-24: Projects

An unordered list of open source :material-open-source-initiative: projects that I contribute/d to:

!!! help "shortcuts"
    Type ++s++ or ++f++ to :octicons-search-16: **search** this site.
    Type ++p++ or ++comma++ to go the :material-skip-previous: **previous** page.
    Type ++n++ or ++period++ to go the **next** :material-skip-next: page.

## [Tiny Weather Forecast Germany (TWFG)](https://tinyweatherforecastgermanygroup.gitlab.io/index/index.html)

!!! info "about"
    An android open source weather forecast app written in :fontawesome-brands-java: `java` focused on Germany using open data provided by Deutscher Wetterdienst (DWD). Being a member of the World Meterological Organization (WMO), DWD also provides weather data shared by other WMO members. Please see the map below for all covered weather stations.

    :material-lightbulb-on: Compared to other open source weather apps on the :simple-fdroid: F-Droid store TinyWeatherForecastGermany does support the data exchange with :material-watch: smart gadgets via [**Gadgetbridge**](https://gadgetbridge.org/) and does **not** rely on the rate-limited [**OpenWeatherMap**](https://openweathermap.org/price) api. :fontawesome-regular-face-grin: 

**Maintainer**: Pawel Dube ([@Starfish](https://codeberg.org/Starfish)) :octicons-star-16: :material-fish:

[:simple-codeberg: code repository](https://codeberg.org/Starfish/TinyWeatherForecastGermany/){ .md-button }

[:simple-fdroid: F-Droid Store page](https://f-droid.org/packages/de.kaffeemitkoffein.tinyweatherforecastgermany){ .md-button }

[:material-home-search: searchable table of all warning *areas* in Germany](https://tinyweatherforecastgermanygroup.gitlab.io/index/areas.html)

[:material-home-search: searchable table of all weather **stations**](https://tinyweatherforecastgermanygroup.gitlab.io/index/stations.html)

[:material-map-plus: OpenStreetMap based **map** (using :simple-folium: `folium`) visualizing all data sources used by TWFG](https://tinyweatherforecastgermanygroup.gitlab.io/index/map.html)

[:material-frequently-asked-questions: list of Frequently asked questions (**FAQ**)](https://tinyweatherforecastgermanygroup.gitlab.io/index/index.html#faq)

[:octicons-code-24: :material-help: javadoc **code documentation**](https://tinyweatherforecastgermanygroup.gitlab.io/twfg-javadoc/index.html)

[:simple-weblate: Weblate contributors](https://tinyweatherforecastgermanygroup.gitlab.io/twfg-contribution-analytics/weblate.html)

??? info "note"
    The **javadoc** docs and **map** are both automatically updated **once daily**.

    The docs also contain :fontawesome-solid-diagram-project: **UML** diagrams generated using [graphviz](https://gitlab.com/graphviz/graphviz) via [PlantUML](https://plantuml.com/) integrated in the [uml-java-doclet](https://github.com/talsma-ict/umldoclet).

### :octicons-mirror-24: Mirrors

!!! info "note"
    The following git repositories are updated **once daily** at 5am UTC.

    Target: increased SEO-Scores, leading interested members of the public to the 'main' project at [:simple-codeberg: codeberg.org](https://codeberg.org/Starfish/TinyWeatherForecastGermany/) the home of the user and developer communities of TinyWeatherForecastGermany.

    During peak times an additional pipeline schedule covering every 8h may be manually activated.

[:material-gitlab: **GitLab** Mirror](https://gitlab.com/tinyweatherforecastgermanygroup/TinyWeatherForecastGermany)[^1]

[:material-github: **GitHub** Mirror](https://github.com/tinyweatherforecastgermanygroup/TinyWeatherForecastGermany)[^1]

[:material-git: **framagit** Mirror](https://framagit.org/tinyweatherforecastgermanygroup/tinyweatherforecastgermanymirror)[^1]

[:simple-gitea: **Gitea** Mirror](https://gitea.com/tinyweatherforecastgermanygroup/TinyWeatherForecastGermanyMirror)[^1]

If you'd like to add a **new** mirror repository on a hosted Forgejo, GitLab, GitHub Enterprise, Gitea, Gogs instance or any other :fontawesome-brands-git: server please get in touch with me ([@eUgEntOptIc44](https://codeberg.org/eUgEntOptIc44)) :material-email:.

### :material-list-status: Status

A [**status** page](https://tinyweatherforecastgermanygroup.github.io/statuspage/) at [GitHub](https://github.com/tinyweatherforecastgermanygroup/statuspage) Pages powered by [upptime](https://upptime.js.org/) and GitHub shared runners is also provided.

### :material-translate: Translations

Translations of **TinyWeatherForecastGermany** are managed on the :simple-weblate: **Weblate**[^2] server provided by :simple-codeberg: Codeberg e.V.

[![TinyWeatherForecastGermany weblate translation status](https://translate.codeberg.org/widgets/tiny-weather-forecast-germany/-/multi-blue.svg "weblate translation status of TinyWeatherForecastGermany"){ loading="lazy" decoding="async" width="400px" height="180px" }](https://translate.codeberg.org/engage/tiny-weather-forecast-germany/){ title="weblate translation status" }

## [Hypatia](https://gitlab.com/divested-mobile/hypatia)

!!! info "about"
    An open source android :fontawesome-brands-java: `java` virus scanner app using hash signatures from several open sources.

Maintainer: Tad ([@IratePorcupine](https://gitlab.com/IratePorcupine))

[:material-gitlab: code repository](https://gitlab.com/divested-mobile/hypatia){ .md-button }

[:simple-fdroid: F-Droid page](https://f-droid.org/packages/us.spotco.malwarescanner/){ .md-button }

### :material-translate: Translations

* Afrikaans
* Finnish
* French[^3]
* German
* Italian
* Portuguese
* Polish
* Spanish
* Russian
* Turkish

## [Currencies](https://github.com/sal0max/currencies)

!!! info "about"
    An open source android :material-language-kotlin: `kotlin` currency converter app. It offers a broad choice of [data sources](#data-sources) to choose from.

Maintainer: [@sal0max](https://github.com/sal0max)

[:material-github: code repository](https://github.com/sal0max/currencies){ .md-button }

[:simple-googleplay: play store page](https://play.google.com/store/apps/details?id=de.salomax.currencies){ .md-button }

[:simple-fdroid: F-Droid page](https://f-droid.org/packages/de.salomax.currencies){ .md-button }

### :material-translate: Translations

Translations of :material-currency-eur::material-currency-gbp::material-currency-usd::material-currency-jpy: **Currencies** are managed on the :simple-weblate: Weblate[^2] server provided by :simple-codeberg: Codeberg e.V.

[![currencies weblate translation status](https://translate.codeberg.org/widgets/currencies/-/multi-blue.svg "weblate translation status of currencies"){ loading="lazy" decoding="async" width="400px" height="180px" }](https://translate.codeberg.org/engage/currencies){ title="weblate translation status" }

### :octicons-database-16: Data sources

* [exchangerate.host :octicons-link-external-16:](https://exchangerate.host/#/#faq) providing over 160 currencies
* [frankfurter.app :octicons-link-external-16:](https://www.frankfurter.app/docs/) providing over 30 currencies --> data provided by the European Central Bank (ECB)
* ...

All sources named above rely (partialy) on the ECB see [here for more information](https://www.ecb.europa.eu/stats/policy_and_exchange_rates/euro_reference_exchange_rates/html/index.en.html) on their terms and conditions concerning data usage, privacy, etc.

To learn about all available APIs and to retrieve more insights concerning currency rate APIs in general please visit the [:material-github: GitHub repository](https://github.com/sal0max/currencies/blob/2f499f4cc4e146b37cc2063a0d46a34ab8b868b0/API.md).

## Simple Chess Clock

!!! info "about"
    Simple Chess Clock is what its name implies: a :fontawesome-regular-chess-king: :fontawesome-regular-chess-queen: :material-timer: chess clock. It aims to be easy to use and easy to read, while also providing some reasonably expected features. If you experience problems on your device, please visit the :material-github: [GitHub](https://github.com/simenheg/simple-chess-clock) repository to report a bug. Please open a new issue there or contribute to an existing one.

Maintainer: Simen Heggestøyl ([@simenheg](https://github.com/simenheg))

[:material-github: code repository](https://github.com/simenheg/simple-chess-clock){ .md-button }

[:simple-fdroid: F-Droid page](https://f-droid.org/packages/com.chessclock.android){ .md-button }

### :material-translate: Translations

Translations of **Simple Chess Clock** were managed on the :simple-weblate: weblate[^2] instance maintained by Marcus Hoffmann ([@Bubu](https://bubu1.eu/)).

I regularly updated the **German** and **French** translations of the app.

## Podverse

!!! info "about"
    Podverse is a free and open source :material-podcast: podcast manager for Android, iOS, and the web. Check it out at [podverse.fm](https://podverse.fm)!

### :material-translate: Translations

Translations of **Podverse** are managed on the :simple-weblate: 'hosted' Weblate[^2] instance.

[![simple-chess-clock weblate translation status](https://hosted.weblate.org/widgets/podverse/-/multi-blue.svg "weblate translation status of podverse"){ loading="lazy" decoding="async" width="400px" height="180px" }](https://hosted.weblate.org/engage/podverse){ title="weblate translation status of podverse" }

I added the **German** translations for the Podverse mobile apps and website. I regularly contribute to the **French** and **German** translations.

<!-- abbreviations -->
*[APIs]: Application Programming Interface, e.g. a REST or SOAP API used to communicate with a server via JSON or XML data
*[DWD]: Deutscher Wetterdienst, the German national weather agency similar to NOAA in the US
*[ECB]: European Central Bank
*[FAQ]: Frequently asked questions
*[SaaS]: Software as a Service
*[SEO]: Search Engine Optimization
*[TWFG]: inofficial abbreviation for Tiny Weather Forecast Germany
*[WMO]: World Meterological Organization, an international body of national weather agencies

<!-- footnotes -->
[^1]: maintained by myself using a scheduled GitLab CI/CD job :octicons-rocket-16:
[^2]: an open source SaaS for community translations of apps and websites. It's used by projects like F-Droid, Godot Game Engine, NewPipe and Minetest to manage their translations.
[^3]: My contribution :fontawesome-regular-face-grin:
