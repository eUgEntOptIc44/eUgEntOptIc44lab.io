---
title: Snippets - Jean-Luc Tibaux
description: my personal collection of useful hints and code snippets
---

# :octicons-code-24: Snippets

!!! info ""
    A collection of **hints** and code **snippets** that I collected.

## Forgejo/Gitea repository description

!!! info ""
    Applies to :simple-gitea: [Gitea](https://gitea.com) and :simple-forgejo: [Forgejo](https://forgejo.org) installations :material-arrow-right: e.g. :simple-codeberg: [codeberg.org](https://codeberg.org) and (partly[^1]) the [GitNex](https://gitnex.codeberg.page/) companion app for :simple-android: android.

The following :simple-html5: `html` tags are confirmed to be parsed by :simple-gitea: Gitea and :simple-forgejo: Forgejo when used in the **descriptions** of repositories:

* `#!html <a>` :material-arrow-right: anchor
* `#!html <b>` :material-arrow-right: bold
* `#!html <br>` :material-arrow-right: line break
* `#!html <i>` :material-arrow-right: italic

:simple-markdown: **Markdown** is as of writing **not** being parsed. :material-emoticon-sad-outline:

## Android SDK setup

**environment**: :simple-linux: linux :material-arrow-right: :simple-gnubash: `bash`

!!! info ""
    Meant to be used for a quick setup of a :simple-linux: `linux` dev environment for [Tiny Weather Forecast Germany (TWFG)](index.md#tiny-weather-forecast-germany-twfg).

```bash
sudo apt -y install openjdk-8-jdk-headless
```

```bash
wget "https://dl.google.com/android/repository/commandlinetools-linux-7583922_latest.zip"
mkdir -p Android/Sdk
unzip "commandlinetools-linux-7583922_latest.zip" -d Android/Sdk

export ANDROID_HOME=$(pwd)/Android/Sdk/cmdline-tools
export PATH="$ANDROID_HOME/bin:$ANDROID_HOME/lib:$ANDROID_HOME/emulator:$ANDROID_HOME/patcher:$ANDROID_HOME/platform-tools:$ANDROID_HOME/tools:$PATH"

yes | sdkmanager --update --sdk_root=${ANDROID_HOME}
sdkmanager --list --sdk_root=${ANDROID_HOME} | grep "build-tools"
yes | sdkmanager "build-tools;29.0.3" "platforms;android-29" "sources;android-29" --sdk_root=${ANDROID_HOME}
yes | sdkmanager --licenses --sdk_root=${ANDROID_HOME}
```

```bash
sudo apt -y install gradle
```

## code search using [dezip.org](https://www.dezip.org/)

!!! info ""
    To :octicons-search-16: search the contents of :material-git: code repositories using :octicons-archive-16: archives containing their source code.

### update the [cached](https://www.dezip.org/https://codeberg.org/Starfish/TinyWeatherForecastGermany/archive/master.tar.gz) tar archive

1. :octicons-trash-16: **remove** cached contents :material-arrow-right: `GET` request: <https://www.dezip.org/https://codeberg.org/Starfish/TinyWeatherForecastGermany/archive/master.tar.gz?remove>

2. :material-update: **update** contents :material-arrow-right: `GET` request: <https://www.dezip.org/https://codeberg.org/Starfish/TinyWeatherForecastGermany/archive/master.tar.gz>

3. :octicons-hourglass-16: please be patient. The page will **reload** when dezip completed the :octicons-package-dependents-16: extraction of the archive's contents.

???+ warning "warning"
    **Please use the `update` function responsible and if possible not more than once per day. This services operates on a :octicons-people-16: fair use policy with :material-speedometer-slow: limited bandwith. Please don't make the creator turning it into a :fontawesome-solid-dollar-sign: paid service with :material-robot-off: captchas, :material-server-security: geoblocking, etc. by :material-fire: wasting bandwidth. :fontawesome-solid-face-angry: Also [see here for more information](https://web.archive.org/web/20221224064343/https://codeberg.org/Codeberg/Community/issues/379#issuecomment-237979) :octicons-link-external-16:.**

???+ info "note"
    This :material-update: update workflow is executed :octicons-stopwatch-16: regularly by an external :material-robot: cron job. So in general a **manual** update should **not** be required.

### [search](https://www.dezip.org/v1/9/https/codeberg.org/Starfish/TinyWeatherForecastGermany/archive/master.tar.gz/tinyweatherforecastgermany/) cached repository contents

Visit <https://www.dezip.org/v1/9/https/codeberg.org/Starfish/TinyWeatherForecastGermany/archive/master.tar.gz/tinyweatherforecastgermany/> using your :fontawesome-brands-firefox-browser: :material-google-chrome: :material-microsoft-edge: browser of choice to search the contents.

!!! info ""
    Type ++f++ to **search** and :material-skip-previous: ++k++ or ++j++ :material-skip-next: to  **change** between :octicons-search-16: search results.

## setup Elixir on Ubuntu focal

```bash
# source: https://gist.github.com/tgroshon/6bcd5b5f5ef6052e1190ca3ec1a378e7

# Install the Erlang Solutions apt key
wget https://packages.erlang-solutions.com/ubuntu/erlang_solutions.asc
sudo apt-key add erlang_solutions.asc

sudo echo 'deb https://packages.erlang-solutions.com/ubuntu focal contrib' | sudo tee /etc/apt/sources.list.d/erlang-solutions.list
sudo apt update
sudo apt install esl-erlang elixir
```

```bash
# for SASS builds
sudo apt install -y rsync
sudo apt install -y inotify-tools
sudo apt install -y sassc
```

<!-- abbreviations -->
*[DWD]: Deutscher Wetterdienst, the German national weather agency similar to NOAA in the US
*[TWFG]: inofficial abbreviation for Tiny Weather Forecast Germany

<!-- footnotes -->
[^1]: Not yet supported in list views only in repository details screen.
